package com.bignerdranch.android.diary;

import android.location.Location;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Kristoffer Duenas
 */

public class Diary {

    private UUID mId;
    private String mTitle;
    private Date mDate;
    private String mDiaryType;
    private String mActivity;
    private String mDuration;
    private String mComment;
    private String mLatitude;
    private String mLongitude;
    private Location mLocation;

    private ArrayList<String> mDiaryTypeList = new ArrayList<>();


    public int getDiaryTypeList() {
        return mDiaryTypeList.indexOf(mDiaryType);
    }


    public Diary() {
        this(UUID.randomUUID());
    }

    public Diary(UUID id) {
        mId = id;
        mDate = new Date();
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDiaryType() {
        return mDiaryType;
    }

    public void setDiaryType(int DiaryType) {
        mDiaryType = mDiaryTypeList.get(DiaryType);
    }

    public void setDiaryType(String diaryType) {
        mDiaryType = diaryType;
    }

    public String getActivity() {
        return mActivity;
    }

    public void setActivity(String Activity) {
        mActivity = Activity;
    }

    public String getDuration() {
        return mDuration;
    }

    public void setDuration(String Duration) {
        mDuration = Duration;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String Comment) {
        mComment = Comment;
    }

    public String getPhotoFilename() {
        return "IMG_" + getId().toString() + ".jpg";
    }

    public Location getLocation() {
        return mLocation;
    }

    public void setLocation(Location location) {
        mLocation = location;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String Longitude) {
        mLongitude = Longitude;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String Latitude) {
        mLatitude = Latitude;
    }

}
