package com.bignerdranch.android.diary.Database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.bignerdranch.android.diary.Settings;

import java.util.UUID;

/**
 * Created by Kristoffer Duenas
 */


public class SettingsCursorWrapper extends CursorWrapper {

    public SettingsCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Settings getSettings() {
        String uuidString = getString(getColumnIndex(SettingsDbSchema.SettingsTable.Cols.UUID));
        String name = getString(getColumnIndex(SettingsDbSchema.SettingsTable.Cols.NAME));
        String SID = getString(getColumnIndex(SettingsDbSchema.SettingsTable.Cols.SID));
        String email = getString(getColumnIndex(SettingsDbSchema.SettingsTable.Cols.EMAIL));
        String gender = getString(getColumnIndex(SettingsDbSchema.SettingsTable.Cols.GENDER));
        String comment = getString(getColumnIndex(SettingsDbSchema.SettingsTable.Cols.COMMENT));

        Settings setting = new Settings(UUID.fromString(uuidString));
        setting.setName(name);
        setting.setSID(SID);
        setting.setEmail(email);
        setting.setGender(gender);
        setting.setComment(comment);

        return setting;

    }

}
