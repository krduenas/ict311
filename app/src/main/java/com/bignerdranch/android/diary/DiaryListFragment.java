package com.bignerdranch.android.diary;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bignerdranch.android.diary.R;

import java.util.List;

/**
 * Created by Kristoffer Duenas
 */


public class DiaryListFragment extends Fragment {

    private RecyclerView mDiaryRecyclerView;
    private DiaryAdapter mAdapter;
    private Button mNewButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_diary_list, container, false);
        mDiaryRecyclerView = (RecyclerView) view
                .findViewById(R.id.diary_recycler_view);
        mDiaryRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_diary_list, menu);
        inflater.inflate(R.menu.fragment_settings_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_diary:
                Diary diary = new Diary();
                DiaryLab.get(getActivity()).addDiary(diary);
                Intent intent = MainActivity
                        .newIntent(getActivity(), diary.getId());
                startActivity(intent);
                return true;
            case R.id.menu_item_settings_diary:
                Settings settings = new Settings();
                SettingsLab.get(getActivity()).addSettings(settings);
                Intent intent1 = SettingsActivity
                        .newIntent(getActivity(), settings.getId());
                startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void updateUI() {
        DiaryLab diaryLab = DiaryLab.get(getActivity());
        List<Diary> diary = diaryLab.getDiary();

        if (mAdapter == null) {
            mAdapter = new DiaryAdapter(diary);
            mDiaryRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setDiary(diary);
            mAdapter.notifyDataSetChanged();
        }
    }



    private class DiaryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Diary mDiary;
        private TextView mTitleTextView;
        private TextView mDateTextView;
        private TextView mActivityTextView;
        private TextView mDurationTextView;
        private TextView mCommentTextView;



        public DiaryHolder(View itemView) {

            super(itemView);
            itemView.setOnClickListener(this);
            mTitleTextView = (TextView) itemView.findViewById(R.id.list_item_diary_title_text_view);
            mDateTextView = (TextView) itemView.findViewById(R.id.list_item_diary_date_text_view);
            mActivityTextView = (TextView) itemView.findViewById(R.id.list_item_diary_activity_text_view);
            //mDurationTextView = (TextView) itemView.findViewById(R.id.list_item_diary_duration_text_view);
            //mCommentTextView = (TextView) itemView.findViewById(R.id.list_item_diary_comment_text_view);

        }

        public void bindDiary(Diary diary) {
            mDiary = diary;
            mTitleTextView.setText(mDiary.getTitle());
            mDateTextView.setText(mDiary.getDate().toString());
            mActivityTextView.setText(mDiary.getActivity());
           // mDurationTextView.setText(mDiary.getDuration());
           // mCommentTextView.setText(mDiary.getComment());
        }

        public void onClick(View v) {

            Intent intent = MainActivity.newIntent(getActivity(), mDiary.getId());
            startActivity(intent);
        }
    }

    private class DiaryAdapter extends RecyclerView.Adapter<DiaryHolder> {

        private List<Diary> mDiary;

        public DiaryAdapter(List<Diary> diary) {
            mDiary = diary;
        }

        @Override
        public DiaryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_diary, parent, false);
            return new DiaryHolder(view);
        }
        @Override
        public void onBindViewHolder(DiaryHolder holder, int position) {
            Diary diary = mDiary.get(position);
            holder.bindDiary(diary);
        }
        @Override
        public int getItemCount() {
            return mDiary.size();
        }

        public void setDiary(List<Diary> diary) {
            mDiary = diary;
        }
    }
}
