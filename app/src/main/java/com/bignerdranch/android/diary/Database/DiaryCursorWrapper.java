package com.bignerdranch.android.diary.Database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.bignerdranch.android.diary.Diary;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Kristoffer Duenas
 */


public class DiaryCursorWrapper extends CursorWrapper {

    public DiaryCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Diary getDiary() {
        String uuidString = getString(getColumnIndex(DiaryDbSchema.DiaryTable.Cols.UUID));
        String title = getString(getColumnIndex(DiaryDbSchema.DiaryTable.Cols.TITLE));
        long date = getLong(getColumnIndex(DiaryDbSchema.DiaryTable.Cols.DATE));
        String diaryType = getString(getColumnIndex(DiaryDbSchema.DiaryTable.Cols.DIARY_TYPE));
        String activity = getString(getColumnIndex(DiaryDbSchema.DiaryTable.Cols.ACTIVITY));
        String duration = getString(getColumnIndex(DiaryDbSchema.DiaryTable.Cols.DURATION));
        String comment = getString(getColumnIndex(DiaryDbSchema.DiaryTable.Cols.COMMENT));
        String longitude = getString(getColumnIndex(DiaryDbSchema.DiaryTable.Cols.LONGITUDE));
        String latitude = getString(getColumnIndex(DiaryDbSchema.DiaryTable.Cols.LATITUDE));

        Diary diary = new Diary(UUID.fromString(uuidString));
        diary.setTitle(title);
        diary.setDate(new Date(date));
        diary.setDiaryType(diaryType);
        diary.setActivity(activity);
        diary.setDuration(duration);
        diary.setComment(comment);
        diary.setLongitude(longitude);
        diary.setLatitude(latitude);

        return diary;

    }
}
