package com.bignerdranch.android.diary;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.UUID;

/**
 * Created by Kristoffer Duenas
 */

public class MainActivity extends SingleFragmentActivity {

    public static final String EXTRA_DIARY_ID = "com.example.diary_id";
    private static final int REQUEST_ERROR = 0;

    public static Intent newIntent(Context packageContext, UUID diaryId) {
        Intent intent = new Intent(packageContext, MainActivity.class);
        intent.putExtra(EXTRA_DIARY_ID, diaryId);
        return intent;
    }


    @Override
    protected Fragment createFragment() {

        UUID diaryId = (UUID) getIntent().getSerializableExtra(EXTRA_DIARY_ID);
        return DiaryFragment.newInstance(diaryId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        int errorCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if(errorCode != ConnectionResult.SUCCESS) {
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(errorCode, this, REQUEST_ERROR,
                    new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            finish();
                        }
                    });
            errorDialog.show();;
        }
    }
}
