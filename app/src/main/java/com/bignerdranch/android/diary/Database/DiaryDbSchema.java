package com.bignerdranch.android.diary.Database;

/**
 * Created by Kristoffer Duenas
 */


public class DiaryDbSchema {

    public static final class DiaryTable {
        public static final String NAME = "Diary";

        public static final class Cols {
            public static final String UUID = "uuid";
            public static final String TITLE = "title";
            public static final String DATE = "date";
            public static final String DIARY_TYPE = "diary_type";
            public static final String ACTIVITY = "activity";
            public static final String DURATION = "duration";
            public static final String COMMENT = "comment";
            public static final String LATITUDE = "latitude";
            public static final String LONGITUDE = "longitude";

        }
    }
}
