package com.bignerdranch.android.diary.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Kristoffer Duenas
 */


public class DiaryBaseHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "diaryBase.db";

    public DiaryBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + DiaryDbSchema.DiaryTable.NAME + "(" + "" +
                "_id integer primary key autoincrement, " +
                DiaryDbSchema.DiaryTable.Cols.UUID + ", " +
                DiaryDbSchema.DiaryTable.Cols.TITLE + ", " +
                DiaryDbSchema.DiaryTable.Cols.DATE + ", " +
                DiaryDbSchema.DiaryTable.Cols.DIARY_TYPE + ", " +
                DiaryDbSchema.DiaryTable.Cols.ACTIVITY + ", " +
                DiaryDbSchema.DiaryTable.Cols.DURATION + ", " +
                DiaryDbSchema.DiaryTable.Cols.COMMENT + ", " +
                DiaryDbSchema.DiaryTable.Cols.LATITUDE + ", " +
                DiaryDbSchema.DiaryTable.Cols.LONGITUDE + ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
