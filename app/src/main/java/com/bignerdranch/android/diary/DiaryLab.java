package com.bignerdranch.android.diary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import com.bignerdranch.android.diary.Database.DiaryBaseHelper;
import com.bignerdranch.android.diary.Database.DiaryDbSchema;
import com.bignerdranch.android.diary.Database.DiaryCursorWrapper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Kristoffer Duenas
 */


public class DiaryLab {

    private static DiaryLab sDiaryLab;
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static DiaryLab get(Context context) {
        if (sDiaryLab == null) {
            sDiaryLab = new DiaryLab(context);
        }
        return sDiaryLab;
    }
    private DiaryLab(Context context) {
        mContext = context.getApplicationContext();
        mDatabase = new DiaryBaseHelper(mContext).getWritableDatabase();

    }

    public List<Diary> getDiary() {

        List<Diary> diary = new ArrayList<>();

        DiaryCursorWrapper cursor = queryDiary(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                diary.add(cursor.getDiary());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return diary;
    }
    public Diary getDiary(UUID id) {


        DiaryCursorWrapper cursor = queryDiary(
                DiaryDbSchema.DiaryTable.Cols.UUID + " = ?",
                new String[] {
                        id.toString()
                }
        );

        try {
            if (cursor.getCount() == 0) {
                return null;
            }

            cursor.moveToFirst();
            return cursor.getDiary();
        } finally {
            cursor.close();
        }

//        return null;
    }


    public void addDiary (Diary t) {

        ContentValues values = getContentValues(t);


        mDatabase.insert(DiaryDbSchema.DiaryTable.NAME, null, values);
        updateDiary(t);
    }

    public void updateDiary(Diary diary) {
        Log.d("aa", "updateDiary: " + diary.getTitle());
        String uuidString = diary.getId().toString();
        ContentValues values = getContentValues(diary);

        mDatabase.update(DiaryDbSchema.DiaryTable.NAME, values,
                DiaryDbSchema.DiaryTable.Cols.UUID + " = ?",
                new String[] {uuidString});
    }

    public void deleteDiary (Diary diary) {
        String uuidString = diary.getId().toString();
        mDatabase.delete(
                DiaryDbSchema.DiaryTable.NAME,
                DiaryDbSchema.DiaryTable.Cols.UUID + " = ?",
                new String[] {uuidString}
        );
    }

    private static ContentValues getContentValues(Diary diary) {
        ContentValues values = new ContentValues();
        values.put(DiaryDbSchema.DiaryTable.Cols.UUID, diary.getId().toString());
        values.put(DiaryDbSchema.DiaryTable.Cols.TITLE, diary.getTitle());
        values.put(DiaryDbSchema.DiaryTable.Cols.DATE, diary.getDate().getTime());
        values.put(DiaryDbSchema.DiaryTable.Cols.DIARY_TYPE, diary.getDiaryType());
        values.put(DiaryDbSchema.DiaryTable.Cols.ACTIVITY, diary.getActivity());
        values.put(DiaryDbSchema.DiaryTable.Cols.DURATION, diary.getDuration());
        values.put(DiaryDbSchema.DiaryTable.Cols.COMMENT, diary.getComment());
        values.put(DiaryDbSchema.DiaryTable.Cols.LONGITUDE, diary.getLongitude());
        values.put(DiaryDbSchema.DiaryTable.Cols.LATITUDE, diary.getLatitude());

        return values;
    }
    private DiaryCursorWrapper queryDiary(String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(
                DiaryDbSchema.DiaryTable.NAME,
                null, // Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, //having
                null // orderBy
        );

//        return cursor;
        return new DiaryCursorWrapper(cursor);
    }

    public File getPhotoFile(Diary diary) {
        File externalFilesDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        if (externalFilesDir == null) {
            return null;
        }

        return new File(externalFilesDir, diary.getPhotoFilename());
    }

}
