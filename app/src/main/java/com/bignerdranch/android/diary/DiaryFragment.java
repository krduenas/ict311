package com.bignerdranch.android.diary;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bignerdranch.android.diary.R;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.File;
import java.util.UUID;

import static android.content.ContentValues.TAG;

/**
 * Created by Kristoffer Duenas
 */


public class DiaryFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private static final String ARG_DIARY_ID = "diary_id";
    private static final int REQUEST_PHOTO = 0;

    private Diary mDiary;
    private EditText mTitleField;
    private Button mDateButton;
    private Spinner mDiaryType;
    private EditText mActivity;
    private EditText mDuration;
    private EditText mComment;
    private Button mDeleteButton;
    private Button mSaveButton;
    private Button mCancelButton;
    private ImageButton mPhotoButton;
    private ImageView mPhotoView;
    private File mPhotoFile;
    private GoogleApiClient mClient;
    private Button mLocationButton;



    public static DiaryFragment newInstance(UUID diaryId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_DIARY_ID, diaryId);

        DiaryFragment fragment = new DiaryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        UUID diaryId = (UUID) getActivity().getIntent().getSerializableExtra(MainActivity.EXTRA_DIARY_ID);
        mDiary = DiaryLab.get(getActivity()).getDiary(diaryId);

        mPhotoFile = DiaryLab.get(getActivity()).getPhotoFile(mDiary);

        mClient = new GoogleApiClient.Builder(getActivity()).addApi(LocationServices.API)
                .build();

        mClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected (Bundle bundle){
                getActivity().invalidateOptionsMenu();
                onConnectedLocation();

            }
            @Override
            public void onConnectionSuspended (int i){
            }
        })
                .build();
    }

    private void onConnectedLocation() {
        LocationRequest request = LocationRequest.create();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setNumUpdates(1);
        request.setInterval(0);

        getActivity().getBaseContext();
        if (ActivityCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission
                        .ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

            LocationServices.FusedLocationApi.requestLocationUpdates(mClient, request,
                    new LocationListener() {
                @Override
                public void onLocationChanged(Location location){
                    Log.d(TAG, "You are at: " + location);

                    mDiary.setLatitude(Double.toString(location.getLatitude()));
                    mDiary.setLongitude(Double.toString(location.getLongitude()));

                    Log.d(TAG, "onConnectedLocation() Latitude location in database: " +
                            mDiary.getLatitude());
                    Log.d(TAG, "onConnectedLocation() Longitude location in database: " +
                            mDiary.getLongitude());
                    updateLocation();
                }
            });

        }
    private void updateLocation() {
        mLocationButton.setText("Latitude: " + mDiary.getLatitude() + ", Longitude: " +
        mDiary.getLongitude());
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().invalidateOptionsMenu();
        mClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        DiaryLab.get(getActivity()).updateDiary(mDiary);
    }

    @Override
    public void onStop() {
        super.onStop();
        mClient.disconnect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_diary, container, false);

        mTitleField = (EditText) v.findViewById(R.id.diary_title);
        mTitleField.setText(mDiary.getTitle());
        mTitleField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mDiary.setTitle(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("test", "on text change" + mDiary.getTitle());
            }
        });

        mDateButton = (Button) v.findViewById(R.id.diary_date);
        mDateButton.setText(mDiary.getDate().toString());
        mDateButton.setEnabled(false);


        mDeleteButton = (Button) v.findViewById(R.id.diary_delete);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(),
                        R.string.delete_toast, Toast.LENGTH_SHORT).show();
                DiaryLab.get(getActivity()).deleteDiary(mDiary);
                getActivity().finish();
            }
        });


        mSaveButton = (Button) v.findViewById(R.id.diary_save);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),
                        R.string.save_toast, Toast.LENGTH_SHORT).show();
                DiaryLab.get(getActivity()).updateDiary(mDiary);
                getActivity().finish();
            }
        });

        mCancelButton = (Button) v.findViewById(R.id.diary_cancel) ;
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DiaryLab.get(getActivity()).deleteDiary(mDiary);
                getActivity().finish();
            }
        });



        mComment = (EditText) v.findViewById(R.id.diary_comment);
        mComment.setText(mDiary.getComment());
        mComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mDiary.setComment(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mActivity = (EditText) v.findViewById(R.id.diary_activity);
        mActivity.setText(mDiary.getActivity());
        mActivity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mDiary.setActivity(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mDuration = (EditText) v.findViewById(R.id.diary_duration);
        mDuration.setText(mDiary.getDuration());
        mDuration.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(
                    CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int count) {
                mDiary.setDuration(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mDiaryType = (Spinner) v.findViewById(R.id.diary_type_spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.diary_type_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
// Apply the adapter to the spinner
        mDiaryType.setAdapter(adapter);


        PackageManager packageManager = getActivity().getPackageManager();

        mPhotoButton = (ImageButton) v.findViewById(R.id.diary_camera);
        final Intent captureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        boolean canTakePhoto = mPhotoFile != null &&
                captureImage.resolveActivity(packageManager) != null;
        mPhotoButton.setEnabled(canTakePhoto);

        if (canTakePhoto) {
            Uri uri = Uri.fromFile(mPhotoFile);
            captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        }

        mPhotoButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivityForResult(captureImage, REQUEST_PHOTO);

            }
        });

        mPhotoView = (ImageView) v.findViewById(R.id.diary_photo);
        updatePhotoView();


        mLocationButton = (Button) v.findViewById(R.id.diary_location_button);
        mLocationButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // Create a Uri from an intent string. Use the result to create an Intent.
                String uriBegin = "geo:" +mDiary.getLatitude() + "," +mDiary.getLongitude();
                String query = mDiary.getLatitude() + mDiary.getLongitude();
                String encodedQuery = Uri.encode(query);
                String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
                Uri uri = Uri.parse(uriString);
//                Uri gmmIntentUri = Uri.parse("geo:" + mDiary.getLatitude() + "," + mDiary.getLongitude());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);

                Log.d(TAG, "onCreateView() Latitude location in database: " + mDiary.getLatitude());
                Log.d(TAG, "onCreateView() Longitude location in database: " + mDiary.getLongitude());
                // Attempt to start an activity that can handle the Intent
                startActivity(intent);
            }
        });

        return v;
    }

    private void updatePhotoView() {
        if (mPhotoFile == null || mPhotoFile.exists()) {
            mPhotoView.setImageDrawable(null);
        } else {
            Bitmap bitmap = PictureUtils.getScaledBitmap(mPhotoFile.getPath(), getActivity());
            mPhotoView.setImageBitmap(bitmap);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        } else if (requestCode == REQUEST_PHOTO) {
            updatePhotoView();
        }
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        Spinner spinner = (Spinner) view.findViewById(R.id.diary_type_spinner);
        spinner.setOnItemSelectedListener(this);
    }

    public void onNothingSelected(AdapterView<?> parent) {

    }

}
