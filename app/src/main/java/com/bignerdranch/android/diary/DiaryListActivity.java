package com.bignerdranch.android.diary;

import android.support.v4.app.Fragment;

/**
 * Created by Kristoffer Duenas
 */


public class DiaryListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new DiaryListFragment();
    }
}
