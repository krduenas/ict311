package com.bignerdranch.android.diary;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;

/**
 * Created by Kristoffer Duenas
 */


public class PictureUtils {

    public static Bitmap getScaledBitmap(String path, Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);

        return getScaledBitmap(path, size.x, size.y);
    }
    public static Bitmap getScaledBitmap(String path, int actWidth, int actHeight) {
// Read in the dimensions of the image on disk
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        float srcWidth = options.outWidth;
        float srcHeight = options.outHeight;
// check how much to scale down by
        int inSampleSize = 1;
        if (srcHeight > actHeight || srcWidth > actWidth) {
            if (srcWidth > srcHeight) {
                inSampleSize = Math.round(srcHeight / actHeight);
            } else {
                inSampleSize = Math.round(srcWidth / actWidth);
            }
        }
        options = new BitmapFactory.Options();
        options.inSampleSize = inSampleSize;
// Read in and create final bitmap
        return BitmapFactory.decodeFile(path, options);
    }

}
